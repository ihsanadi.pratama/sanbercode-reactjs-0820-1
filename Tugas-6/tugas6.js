// Soal 1 
const luasLingkaran = (radius) => {
    return 3.14*radius*radius;
}

console.log(luasLingkaran(4))

const kelilingLingkaran = (radius) => {
    return 3.14*(radius+radius);
}

console.log(kelilingLingkaran(4))


// Soal 2
let kalimat = ""
const firstSentence = 'saya'
const secondSentence = 'adalah'
const thirdSentence = 'seorang'
const fourthSentence = 'frontend'
const fifthSentence = 'developer'

const combinedString = `${firstSentence} ${secondSentence} ${thirdSentence} ${fourthSentence} ${fifthSentence}`
console.log(combinedString);


// Soal 3
const newFunction = (firstName, lastName) => {
    return {firstName, lastName, fullName(){
        console.log(firstName + " " + lastName)
      }
    }
}

// Driver Code 
newFunction("William", "Imoh").fullName()


// Soal 4
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}

const {firstName, lastName, destination, occupation, spell} = newObject

// Driver code
console.log(firstName, lastName, destination, occupation)


// Soal 5
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

// const combined = west.concat(east) ES5

const combined = [...west, ...east] //ES6

//Driver Code
console.log(combined)