// Soal 1
var loopNum = 2;
console.log("LOOPING PERTAMA");

while(loopNum <= 20){
    console.log(loopNum + " - I love coding")
    loopNum += 2;
}

console.log("LOOPING KEDUA");
while(loopNum >= 2){
    loopNum -= 2;
    if(loopNum >=2 ){
        console.log(loopNum + " - I will become a frontend developer");
    }
}

// Soal 2
var forNum = 1;
for(var num = 1; num < 21 ; num++){
    if(num % 3 == 0 && num % 2 == 1){
        console.log(num + " - I Love Coding")
    }else if(num % 2 == 0){
        console.log(num + " - Berkualitas")
    }else if(num % 2 == 1){
        console.log(num + " - Santai")    
    }
}

// Soal 3
var hashtagResult = '';

for(var row = 1; row <= 7; row++){
    for(var col = 1; col <= row; col++){
        hashtagResult += '#';
    }
    console.log(hashtagResult);
    hashtagResult = '';
}

// Soal 4
var kalimat = "saya sangat senang belajar javascript";
console.log(kalimat.split(" "));

// Soal 5
var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];
var sort = daftarBuah.sort();

for(var arr = 0; arr <= sort.length-1; arr++){
    var temp = sort.slice(arr, arr+1);
    console.log(temp.join(" "));
}