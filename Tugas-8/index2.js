// Soal 2
var readBooksPromise = require("./promise.js");

var books = [
  { name: "LOTR", timeSpent: 3000 },
  { name: "Fidas", timeSpent: 2000 },
  { name: "Kalkulus", timeSpent: 4000 },
];

function runReadBooks() {
  readBooksPromise(10000, books[0])
    .then((timeLeft) => {
      return readBooksPromise(timeLeft, books[1]);
    })
    .then((timeLeft) => {
        return readBooksPromise(timeLeft, books[2]);
    })
    .catch((timeLeft) => {
        console.log(timeLeft)
    })
}

runReadBooks();