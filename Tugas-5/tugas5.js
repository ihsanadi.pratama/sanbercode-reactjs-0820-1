// Soal 1
function halo(){
    return "Halo Sanbers!"
}
console.log(halo())


// Soal 2
function kalikan(){
    return num1*num2
}

var num1 = 12
var num2 = 4

var hasilKali = kalikan(num1, num2)
console.log(hasilKali)


// Soal 3
function introduce(name, age, address, hobby){
    return "Nama saya " + name + ", umur saya " + age + " tahun, alamat saya di " + address + ", dan saya punya hobby yaitu " + hobby + "!"
}

var name = "John"
var age = 30
var address = "jalan belum jadi"
var hobby = "Gaming"

var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan)


// Soal 4
var arrayDaftarPeserta = ["Asep", "laki-laki", "baca buku", 1992]
var objDaftarPeserta = {
    nama: 'Asep',
    jenisKelamin: "laki-laki",
    hobi: "baca buku",
    tahunLahir: 1992
}
console.log(objDaftarPeserta)


// Soal 5
var buah = [{nama: "strawberry", warna: "merah", adaBijinya: "tidak", harga: 9000},
            {nama: "jeruk", warna: "oranye", adaBijinya: "ada", harga: 8000},
            {nama: "Semangka", warna: "Hijau & Merah", adaBijinya: "ada", harga: 10000},
            {nama: "Pisang", warna: "Kuning", adaBijinya: "tidak", harga: 5000},
            ]
console.log(buah[0])


// Soal 6
var dataFilm = []
function addDataFilm(dataFilm){
    return dataFilm = [{nama: 'Avengers: Endgame', durasi: '3h 2m', genre: 'Action/Sci-fi', tahun: 2019}]
}
var temp = addDataFilm(dataFilm)
console.log(temp)